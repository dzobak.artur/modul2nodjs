const CronJob = require('cron').CronJob;
const axios = require('axios');

function startPassengerCountJob() {
    const job = new CronJob(
        '*/30 * * * * *', 
        async () => {
            try {
                const response = await axios.get('http://localhost:3007/aeroport/passenger-count');
                console.log(`[passengerCount.job] Total passengers: ${response.data}`);
            } catch (error) {
                console.error(`[passengerCount.job] Error: ${error.message}`);
            }
        },
    );

    job.start();
}

module.exports = startPassengerCountJob;
