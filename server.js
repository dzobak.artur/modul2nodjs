const express = require('express');
const mongoose = require('mongoose');
const { port, mongodb_uri } = require('./config');
const aeroportRoutes = require('./routes/aeroport');
const errorHandler = require('./middlewares/errorHandler');
const createError = require('http-errors');
const startPassengerCountJob = require('./passengerCount.job');


mongoose.connect(mongodb_uri)
  .then(() => {
    console.log('Mongo DB connected');
    startPassengerCountJob();
  });

  const app = express();

  // Built-in middleware that parses incoming requests with JSON payloads
  app.use(express.json());
  
  // Application-level middleware. Executed every time the app receives a request
  app.use((req, res, next) => {
    console.log(`[${new Date().toUTCString()}] ${req.method}: ${req.path}`);
    next();
  });
  
  // An endpoint to hadle base url route GET request
  app.get('/', (req, res) => {
    res.status(200).json({
        status: 200,
        data: {
          message: "Node.js ExApp"
        }
    })
  });
  app.use('/aeroport', aeroportRoutes);

// Application-level middleware. Handling requests for a non-existent path
app.use((req, res, next) => {
  next(createError.NotFound());
});

// Error-handling middleware. Handling global application errors
app.use((err, req, res, next) => {
  const erorrStatus = err.status || 500;
  console.error(`${'\x1b[31m'}[${new Date().toUTCString()}] ${req.method}: ${req.path}. Error(${erorrStatus}): ${err.message}`, '\x1b[0m');
  res.status(erorrStatus).send({
      status: erorrStatus,
      error: err
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
