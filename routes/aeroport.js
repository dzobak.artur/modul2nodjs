const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const aeroportController = require('../controllers/aeroport');

router.post('/import', upload.single('file'), aeroportController.importData);     // TASK 1    Потрібно завантажувати CSV файл.
router.get('/passenger-count', aeroportController.getPassengerCount);             // TASK 2    Кожні 30 секунд виводить кількість пасажирів в консоль.

module.exports = router;
