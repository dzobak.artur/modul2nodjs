const Aeroport = require('../models/aeroport');
const fs = require('fs');
const readline = require('readline');
const createError = require('http-errors');


exports.importData = async (req, res, next) => {
    const filePath = req.file.path;
    const rl = readline.createInterface({
        input: fs.createReadStream(filePath),
        output: process.stdout,
        terminal: false
    });

    const records = [];

    rl.on('line', (line) => {
        const [flight_number, arrival_city, seat_count, flight_time, ticket_price] = line.split(',');
        records.push({
            flight_number,
            arrival_city,
            seat_count: parseInt(seat_count),
            flight_time,
            ticket_price: parseFloat(ticket_price)
        });
    });

    rl.on('close', async () => {
        try {
            await Aeroport.insertMany(records);
            res.send('Data imported successfully');
        } catch (err) {
            next(createError(500, err.message));
        }
    });
};

exports.getPassengerCount = async (req, res, next) => {
    try {
        const totalPassengers = await Aeroport.aggregate([
            { 
                $group: { 
                    _id: null, 
                    total_passengers: { 
                        $sum: { 
                            $convert: { 
                                input: "$seat_count", 
                                to: "int", 
                                onError: 0, 
                                onNull: 0 
                            } 
                        } 
                    } 
                } 
            }
        ]);
        res.send(`Total passengers: ${totalPassengers[0].total_passengers}`);
    } catch (err) {
        next(createError(500, err.message));
    }
};

