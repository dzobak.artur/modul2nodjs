const mongoose = require('mongoose');

const AeroportSchema = new mongoose.Schema({
    flight_number: String,
    arrival_city: String,
    seat_count: String,
    flight_time: String,
    ticket_price: String
});

const Aeroport = mongoose.model('Aeroport', AeroportSchema);

module.exports = Aeroport;
