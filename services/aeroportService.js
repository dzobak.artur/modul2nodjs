const Aeroport = require('../models/aeroport');
const fs = require('fs');
const readline = require('readline');

const importData = (filePath) => {
    return new Promise((resolve, reject) => {
        const rl = readline.createInterface({
            input: fs.createReadStream(filePath),
            output: process.stdout,
            terminal: false
        });

        const records = [];

        rl.on('line', (line) => {
            const [flight_number, arrival_city, seat_count, flight_time, ticket_price] = line.split(',');
            records.push({
                flight_number,
                arrival_city,
                seat_count: parseInt(seat_count),
                flight_time,
                ticket_price: parseFloat(ticket_price)
            });
        });

        rl.on('close', async () => {
            try {
                await Aeroport.insertMany(records);
                resolve('Data imported successfully');
            } catch (err) {
                reject(err.message);
            }
        });
    });
};

const getPassengerCount = async () => {
    const totalPassengers = await Aeroport.aggregate([
        { $group: { _id: null, total_passengers: { $sum: "$seat_count" } } }
    ]);
    return totalPassengers[0].total_passengers;
};

module.exports = {
    importData,
    getPassengerCount
};
